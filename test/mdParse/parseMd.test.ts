import {join} from "path"
import {parseMd} from "../../src/markdownParser/parseMd"
import {getMeta} from "../../src/markdownParser/getMeta"

describe("markdown parser test", ()=>{
	const parsedMarkdown = parseMd(join(__dirname,"../testRes/markdownParse.md"))
	const parsedMeta = getMeta(
		`\`\`\`meta
@title The best guide webpage
@slug best-guide-webpage
@author.name John Doe
@author.email john.doe@example.com
\`\`\``)

	it("should parse the comment meta correctly",()=>{
		expect(parsedMarkdown.meta.slug).toBe("best-guide-webpage")
		expect(parsedMarkdown.meta.title).toBe("The best guide webpage")
		expect(parsedMarkdown.meta.author.name).toBe("John Doe")
		expect(parsedMarkdown.meta.author.email).toBe("john.doe@example.com")
	})

	it("meta should be parsed correctly from hardcoded string",()=>{
		expect(parsedMeta.slug).toBe("best-guide-webpage")
		expect(parsedMeta.title).toBe("The best guide webpage")
		expect(parsedMeta.author.name).toBe("John Doe")
		expect(parsedMeta.author.email).toBe("john.doe@example.com")
	})
	it("should have same meta output for hardcoded string and from parsed markdown", ()=>{
		expect(parsedMarkdown.meta).toStrictEqual(parsedMeta)
	})
})