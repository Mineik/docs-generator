import {parseJSdoc} from "../../src/jsDocExtractor/parse"

describe("JSDoc parser test", ()=>{
	const testParse = parseJSdoc(`
		/**
		 * Testovací JSDoc
		 * @param test
		 * @param {{test2:string}} eh
		 * @param test3 Náhodný Text
		 * @return boolean Success
		 */
	`)
	it("Has the correct description",()=>{
		expect(testParse.description).toBe("Testovací JSDoc")
	})

	it("Has the correct param name parsing", ()=>{
		expect(testParse.params[0].name).toBe("test")
	})
	it("Has the correct param type parsing", ()=>{
		expect(testParse.params[1].type).toBe("string")
	})
	it("Parses description of param correctly", ()=>{
		expect(testParse.params[2].desc).toBe("Náhodný Text")
	})

	it("Parses return type correctly", ()=>{
		expect(testParse.returns.what).toBe("boolean")
		expect(testParse.returns.name).toBe("Success ")
	})
})