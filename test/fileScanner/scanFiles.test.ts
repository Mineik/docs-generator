import {join} from "path"
import Config  from "../../src/opshuns/sharedOptionsHandler"
import {getParsablePaths} from "../../src/getFilesToParse"

beforeAll(()=>{
	global.config = new Config() //var because we access from far away
})

test("list all testResourceFiles", ()=>{
	const rootPath = join(process.cwd(),"/test/testRes")
	const resultFiles = getParsablePaths(rootPath)
	expect(resultFiles.length).toBe(3)
})

test("list all files in project",()=>{
	const files = getParsablePaths()

	expect(files.filter(f=>f.path.startsWith(join(process.cwd(),"/src"))).length).toBeGreaterThan(0)
})