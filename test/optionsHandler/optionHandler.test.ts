import Config from "../../src/opshuns/sharedOptionsHandler"
import {join} from "path"
test("Options Test",()=>{
	const config = new Config()
	const gottenConfig = config.getConfig("excludeDirs")
	expect(gottenConfig.length).toBe(1)
})



test("load test configurations correctly", ()=>{
	const config = new Config(join(__dirname,"../testRes/.docsgenrc"))

	expect(config.getConfig("excludeDirs")[0]).toBe("src")
})

test("return empty object on wrong key",  ()=>{
	const config = new Config()
	expect(config.getConfig("adkfaůdlkf")).toStrictEqual({})
})