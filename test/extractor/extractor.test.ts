import {join} from "path"
import {getFileContents} from "../../src/jsDocExtractor/extract"

test("parser modules", ()=>{
	const load_trash_file = join(__dirname,"/../testRes/extractorTestResource.ts")
	const file = getFileContents(load_trash_file)
	expect(file).toBe("console.log(\"Hello World\")")
})