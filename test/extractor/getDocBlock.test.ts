import {join} from "path"
import {getFileContents, getJSDocComment} from "../../src/jsDocExtractor/extract"

test("get document block", ()=>{
	const file = getFileContents(join(__dirname,"/../testRes/jsdocBlockTestResource.ts"))
	const comments = getJSDocComment(file)
	expect(comments.length).toBeGreaterThanOrEqual(3)
})