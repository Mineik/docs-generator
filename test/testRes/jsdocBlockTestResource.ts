/**
 * This class serves as test resource and shouldnt be used in production code
 * @autor Daniel Brada <dan@hudebniciulicnici.eu>
 */
class jsdocBlocks{
	/**
	 * The testing resource 1
	 * @param blah
	 */
	sampleFunction(blah){
		console.log("Tst")
	}

	/**
	 * Another testing resource
	 * @param sumA rofl
	 * @param sumB hahaxd
	 * @returns trash
	 */
	anotherSampleFunction(sumA, sumB){
		return sumA+sumB
	}
}