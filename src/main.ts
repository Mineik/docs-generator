import {writeFileSync} from "fs"
import {join} from "path"
import {optionDefaults} from "./opshuns/Options"
import {getReactDOMVersion, getReactVersion} from "./docRender/guideGenerator"
import packageJS from "../package.json"
import SharedOptionsHandler from "./opshuns/sharedOptionsHandler"

/**
 * TODO list
 * Parse doc folder
 * Parse src folder and extract jsdoc
 * Parse test folder and extract jsdoc
 * Parse jsdoc
 * Make doc folder markdown and feed it to gatsby
 * Generate documentation from jsdoc and feed it to gatsby
 * Add styling and let custom styling override the default one
 * Create default styling
 * Create react template
 */

const processArgs = process.argv
if(processArgs.includes("--generateOpts")){
	writeFileSync(join(process.cwd(),"/.docsgenrc"),JSON.stringify(optionDefaults), {
		encoding:"utf-8",
	})
	console.log("Configuration generated successfully!")
	process.exit(0) //exit the process, this flag ends the process
}

function main(){
	console.log(`Rendering documentation on docsgen v${packageJS.version} with react version ${getReactVersion()} and React DOM version ${getReactDOMVersion()}`)
	console.log("initialization of config...")
	global.config = new SharedOptionsHandler()
	console.log("done")

}

if(require.main === module) main()