declare type GeneratedDoc = {
	description: string,
	params: param[],
	returns: returns
}

declare type param = {
	name: string | undefined
	type: string | undefined
	desc: string | undefined
}

declare type returns = {
	name: string | undefined
	what: string | undefined
}