import Doctrine from "doctrine"
import {GeneratedDoc} from "./GeneratedDoc"

/**
 * Parses JSdoc comment to format
 * @param docComment entire JSdoc comment
 * @return parsedAST into something easier to work with
 */
export function parseJSdoc(docComment: string): GeneratedDoc{
	const docAST = Doctrine.parse(docComment, {unwrap: true, recoverable: true, tags: ["param", "return","returns"]})
	const generated: GeneratedDoc = new GeneratedDoc()
	generated.description = docAST.description.replace("/**\n","").replace("*/","")
	generated.params = docAST.tags.filter(e=> {
		return e.title == "param"
	}).map(tag=>{
		return {
			name: tag.name,
			//@ts-ignore
			type: tag.type?.fields[0].value.name,
			desc: tag.description
		}
	})
	generated.returns = (()=>{
		const temp = docAST.tags.find(e=>["return","returns"].includes(e.title))
		return{
			name: temp.description.split(" ").map((e,i)=> {
				if(i===0) return ""
				else return e+" "
			}).join("").replace("\n/",""),
			what: temp.description.split(" ")[0],
		}
	})()

	return generated
}
