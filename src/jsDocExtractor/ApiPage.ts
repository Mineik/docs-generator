import {GuideMeta} from "../markdownParser/GuidePage"

/**
 * This modules serves to generate documentation of one module, using JSdoc and
 */
export default class ApiPage{
	meta: ModuleMeta
	content: ApiContent
}

export class ApiContent{
	properties?: ApiContentProperty[] //all properties inside a module
	methods?: ApiMethodProperty[] //
	classes: ApiClassProperty[]
	defaultClass?: ApiClassProperty
	defaultMethod?: ApiMethodProperty
}

export class APIPropertyBase{
	name: string
	isExported?: boolean
}

export class ClassPropertyBase implements APIPropertyBase{
	name: string
	isExported: false = false
	visibility: "public"|"private" = "public"
}


export class ApiContentProperty extends APIPropertyBase{
	type?: string|string[]
	kind?: "var"|"let"|"const"
}

export class ApiMethodProperty extends APIPropertyBase {
	params?: ApiContentProperty[]
	returnType?: string
}

export class ModuleMeta extends GuideMeta{
	moduleHasDefaultClass?: boolean
	moduleHasDefaultMethod?:boolean
}

export class ApiClassProperty extends APIPropertyBase{
	extensionOf: false | string | PartialWebPageRender
	classProps: ClassProperty[]
	internalMethods: ClassInternalMethod[]
}

export class ClassProperty extends ClassPropertyBase implements ApiContentProperty {
	kind = undefined
	redadonly?: boolean
}

export class ClassInternalMethod extends ClassPropertyBase implements ApiMethodProperty{
	visibility: "public"|"private" = "public"
}

export class PartialWebPageRender{
	name: string
	href: string
}
