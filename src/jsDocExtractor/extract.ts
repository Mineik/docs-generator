import {readFileSync} from "fs"

export function getFileContents(filepath: string){
	return readFileSync(filepath, "utf-8")
}

export function getJSDocComment(fileContent: string){
	const _COMMENTREGEXP = /\/\*\*.*?\*\//gms
	return fileContent.match(_COMMENTREGEXP)
}