export class GeneratedDoc implements GeneratedDoc{
	description: string
	params: { name: string | undefined; type: string | undefined; desc: string | undefined }[]
	returns: {
		name: string
		what: string
	}
}