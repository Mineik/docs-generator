import astts from "@typescript-eslint/typescript-estree"
import ApiPage, {ApiClassProperty, ApiContentProperty, ApiMethodProperty} from "./ApiPage"


export function parseModule(code):ApiPage{
	const ApiPageReturnable = new ApiPage()
	const parsedAST = astts.parse(code, {comment:true, range:false, loc: true})
	parsedAST.body.forEach(bodyMember=>{
		if(bodyMember.type=="ExportDefaultDeclaration") {
			if(bodyMember.declaration.type === "ClassDeclaration"){
				ApiPageReturnable.meta.moduleHasDefaultClass = true
				ApiPageReturnable.content.defaultClass = generateClassProperty(bodyMember.declaration)
			}else if(bodyMember.declaration.type=="FunctionDeclaration"){
				ApiPageReturnable.meta.moduleHasDefaultMethod = true
				ApiPageReturnable.content.defaultMethod = generateFunctionProperty(bodyMember.declaration)
			}
		}else if(bodyMember.type == "ExportNamedDeclaration"){
			if(bodyMember.declaration.type=="FunctionDeclaration"){
				ApiPageReturnable.content.methods.push(generateFunctionProperty(bodyMember.declaration, true))
			}
			if(bodyMember.declaration.type == "VariableDeclaration"){
				ApiPageReturnable.content.properties.push(generateProp(bodyMember.declaration.declarations[0],
					bodyMember.declaration.kind,
					true))
			}
			if(bodyMember.declaration.type=="ClassDeclaration"){
				ApiPageReturnable.content.classes.push(generateClassProperty(bodyMember.declaration, true))
			}
		}else{
			if(bodyMember.type=="FunctionDeclaration"){
				ApiPageReturnable.content.methods.push(generateFunctionProperty(bodyMember,false))
			}
			if(bodyMember.type == "VariableDeclaration"){
				ApiPageReturnable.content.properties.push(generateProp(bodyMember.declarations[0],
					bodyMember.kind, false))
			}
			if(bodyMember.type=="ClassDeclaration"){
				ApiPageReturnable.content.classes.push(generateClassProperty(bodyMember))
			}
		}
	})

	return ApiPageReturnable
}

function generateClassProperty(node, isExported=false): ApiClassProperty{
	return{
		name: node.id.name,
		isExported,
		extensionOf: node.superClass? false: node.superClass.name,
		classProps: node.body.body.filter(property=>{
			return property.type == "ClassProperty"
		}).map(prop=>{
			return{
				name: prop.key.name,
				visibility: prop.accessibility,
				readonly: prop.readonly
			}
		}),
		internalMethods: node.body
			.filter(prop=>prop.type=="MethodDefinition"||prop.type=="ClassPrivateMethod")
			.map(prop=>{
				return{
					name:prop.key.name,
					visibility: prop.accessibility
				}
			})
	}

}

function generateFunctionProperty(node, isExported=false): ApiMethodProperty{
	return{
		name: node.id.name,
		isExported,
		params: node.params?node.params.map(param=>{
			return{
				name:param.name,
				type:param.typeAnnotation?.typeAnnotation.type !== "TSTypeReference" ?
					convert(param.typeAnnotation.typeAnnotation.type, param.typeAnnotation.typeAnnotation):
					param.typeAnnotation.typeAnnotation.typeName.name
			}
		}):undefined,
		returnType: node.returnType?.typeAnnotation.type !== "TSTypeReference"?
			convert(node.returnType?.typeAnnotaion.type, node.returnType?.typeAnnotation):
			node.returnType.typeAnnotation.typeName.name
	}
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function generateProp(node, kindNode, isExported=false):ApiContentProperty{
	return{
		name: node.id.name,
		isExported,
		type: node.typeAnnotation?.typeAnnotation.type=="TSTypeReference"?
			node.typeAnnotation.typeAnnotation.typeName.name:
			convert(node.typeAnnotation.typeAnnotation.type, node.typeAnnotation.typeAnnotation),
		kind: kindNode
	}
}

function convert(type:string, typeObject?): string|string[]{
	if(type != "TSUnionType"){
		if(typeObject?.types){
			return typeObject.types.map((type, index)=>{
				if(type.type != "TSTypeReference") return convert(type, typeObject.types[index])
				else return type.typeName.name
			})
		}
	}
	switch (type){
	case "TSStringKeyword": return "string"
	case "TSNumberKeyword": return "number"
	case "TSBooleanKeyword": return "boolean"
	case "TSArrayType":
		if(typeObject.elementType.type != "TSTypeReference") return convert(typeObject.elementType.type,typeObject)+"[]"
		else return typeObject.elementType.typeName.name+"[]"
	default: return "any"
	}
}
