import {readFileSync} from "fs"
import {join} from "path"
import GuidePage from "./GuidePage"
import {getMeta} from "./getMeta"
/*
 META string
 ```meta
 @title  .*
 @slug [a-z\-0-9]*
 @author.name
 @author.email
 ```
 */

const metaRegexp = /```meta.*?```/gs

export function parseMd(path=join(process.cwd(),"/README.md")):GuidePage{
	const file = readFileSync(path, "utf-8")
	const caughtMeta = file.match(metaRegexp)
	return {
		content: file.replace(caughtMeta[0],""),
		meta: getMeta(caughtMeta[0])
	}
}


