export default class GuidePage{
	meta: GuideMeta
	content: string
}

export class GuideMeta{
	title: string
	slug: string
	author?: MetaAuthor
}

export class MetaAuthor{
	name?:string
	email?:string
}