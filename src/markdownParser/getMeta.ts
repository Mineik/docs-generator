import {GuideMeta} from "./GuidePage"
const SlugMatchable = /@slug [a-z\-0-9]*/
const TitleMatchable = /@title .*/
const AuthorNameMatchable = /@author\.name .*/
const AuthorEmailMatchable = /@author\.email .*/

export function getMeta(metaComment: string): GuideMeta{
	const slug = metaComment.match(SlugMatchable)[0].replace("@slug ","")
	const title = metaComment.match(TitleMatchable)[0].replace("@title ","")
	const authorName = metaComment.match(AuthorNameMatchable)[0].replace("@author.name ", "")
	const authorEmail = metaComment.match(AuthorEmailMatchable)[0].replace("@author.email ", "")

	if(authorEmail || authorName){
		if(authorName && authorEmail){
			return {
				title,
				slug,
				author:{
					name: authorName,
					email: authorEmail
				}
			}
		}
		else if(authorName){
			return{
				title,
				slug,
				author:{
					name: authorName
				}
			}
		}else{
			return{
				title,
				slug,
				author:{
					email: authorEmail
				}
			}
		}
	}else{
		return{
			title,slug
		}
	}
}