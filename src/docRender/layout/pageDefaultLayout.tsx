import React from "react"


export default function PageDefaultLayout(props: any){
	return(
		<div className={"layout layout-page"}>

			{props.children}
		</div>
	)
}