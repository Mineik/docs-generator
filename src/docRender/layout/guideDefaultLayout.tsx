import React from "react"
import GuidePage from "../../markdownParser/GuidePage"
import Markdown from "markdown-to-jsx"
import AuthorBox from "./components/AuthorBox"

export default function GuideDefaultLayout(props: { page:GuidePage }){
	return(
		<div className={"layout layout-guide"}>
			<h1>{props.page.meta.title}</h1>
			<AuthorBox meta={props.page.meta}/>
			<div className={"content guide-content"}>
				<Markdown>{props.page.content}</Markdown>
			</div>
		</div>
	)
}