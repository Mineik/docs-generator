import React from "react"
import ApiPage from "../../jsDocExtractor/ApiPage"
import AuthorBox from "./components/AuthorBox"


export default function ApiDefaultLayout(props:{
	page: ApiPage
}){
	return(
		<div className={"layout layout-api"}>
			<h1>Module: {props.page.meta.title}</h1>
			<AuthorBox meta={props.page.meta}/>
			<div className={"content content-api"}>
				
			</div>
		</div>
	)
}