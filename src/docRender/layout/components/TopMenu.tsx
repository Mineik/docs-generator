import React from "react"

export function TopMenu(){
	const [items, setItems] = React.useState([])
	React.useEffect(()=>{
		setItems(global.config.getConfig("topMenuItems"))
	},[])
	return(
		<ul className={"menu menu-top"}>
			{items.map(item=>{
				return(
					<a href={item.href}>
						<li className={"menu-item"}>
							{item.name}
						</li>
					</a>
				)
			})}
		</ul>
	)

}