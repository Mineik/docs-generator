import React from "react"
import {GuideMeta} from "../../../markdownParser/GuidePage"

export default function AuthorBox(props: {meta: GuideMeta}){
	return(
		<>
			{props.meta.author?
				<div className={"authorBox"}>
					{
						props.meta.author.email ?
							<a href={`mailto:${props.meta.author.email}`} className={"authorName"}>{props.meta.author.name ? props.meta.author.name : props.meta.author.name}</a>
							: <span className={"authorName"}>{props.meta.author.name}</span>
					}
				</div>:
				""}
		</>
	)
}