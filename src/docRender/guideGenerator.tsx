import DOM from "react-dom/server"
import GuidePage from "../markdownParser/GuidePage"
import GuideDefaultLayout from "./layout/guideDefaultLayout"
import ApiPage from "../jsDocExtractor/ApiPage"
import ApiDefaultLayout from "./layout/apiDefaultLayout"
import React from "react"
import RenderedWebPage from "./RenderedWebPage"

export function generateListOfPagesToRender(pageCollection: GuidePage[]|ApiPage[]):RenderedWebPage{
	// @ts-ignore
	return pageCollection.map(elem=>{
		if(typeof elem === typeof GuidePage){
			return{
				path: `/guide/${elem.meta.slug}.html`,
				title: elem.meta.title,
				renderedPage: generetaGuide(elem),
			}
		}else if(typeof elem === typeof ApiPage){
			return{
				path: `/api/${elem.meta.slug}.html`,
				title: elem.meta.title,
				renderedPage: generateAPI(elem)
			}
		}
		else return undefined
	})
}


export function generetaGuide(page: GuidePage): string{
	return DOM.renderToString(<GuideDefaultLayout page={page} />)
}

export function generateAPI(page: ApiPage): string{
	return DOM.renderToString(<ApiDefaultLayout page={page} />)
}


export function getReactVersion(){
	return React.version
}

export function getReactDOMVersion(){
	return DOM.version
}