import {Component} from "react"

export default class RenderedWebPage {
	title: string
	path: string
	renderedWebpage: string|Component|Element
}