import glob from "glob"

export function getParsablePaths(pathScanned=process.cwd()): { path: string, type: "code" | "guide" }[]{
	const filetypes = global.config.getConfig("filetypes")
	let files: string[] = glob.sync(`${pathScanned}/**/*.{${filetypes.join(",")},md}`,{})
	files = files.filter(file=>{
		const excludedDirs = global.config.getConfig("excludeDirs")
		const excludedDirectoriesResult = excludedDirs.map(dir=>{
			return file.includes(dir)
		})
		return !excludedDirectoriesResult.includes(true)
	})
	return files.map(file=>{
		return{
			path: file,
			type: isThisCode(file)? "code":"guide"
		}
	})
}

function isThisCode(filename: string):boolean{
	const filetypes = global.config.getConfig("filetypes")
	return filetypes.includes(filename.split(".").pop())
}
