export default class Options{
	filetypes: string[]
	excludeDirs: string[] //will
	topMenuItems: MenuItem[]
}

export const optionDefaults = {
	filetypes: ["js","mjs","jsx","ts","tsx"],
	excludeDirs: ["node_modules"],

}

export class MenuItem{
	title: string
	href: string
}