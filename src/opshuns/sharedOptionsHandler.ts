import {readFileSync} from "fs"
import {join} from "path"
import Options, {optionDefaults} from "./Options"

export default class SharedOptionsHandler{
	private readonly options: Options

	constructor(configFile=join(process.cwd(),"/.docsgenrc")){
		try{
			const file = readFileSync(configFile, "utf-8")
			this.options =  JSON.parse(file)
		}catch(e){
			this.options = optionDefaults
			console.log("Didn't find config file, using default settings.")
		}
	}
	public getConfig(key: string): any{
		const gottenConf = this.options[key]

		if(typeof gottenConf == "undefined") return {}
		else return gottenConf
	}
}