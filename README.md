# Docsgen
it will be able to generate nice guide webpage for you

## Setup
When it is done you can install it through npm. Now it doesn't work

### What works
* Support library
    * Finding files in project - works
    * Finding JSdoc comments - works
    * Parsing JSdoc comments - works, sort of
    * Parsing Markdown for guide page - not implemented yet
* Main app
    * Generating webpage - not implemented yet
    * Override styles with custom styling - not implemented yet